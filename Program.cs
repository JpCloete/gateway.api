using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Ocelot.Middleware;

namespace Gateway.Api
{
    public class Program
    {
        
    public static void Main(string[] args)
    {
        CreateHostBuilder(args).Run();
    }

    public static IWebHost CreateHostBuilder(string[] args) =>
        WebHost.CreateDefaultBuilder(args)
        .ConfigureAppConfiguration((hostingContext, config) =>
        {
            config
            .SetBasePath(hostingContext.HostingEnvironment.ContentRootPath)
            .AddJsonFile("Ocelot.Json.Files/ocelot.Users.Microservice.json", optional: false, reloadOnChange: true);
        })
        .UseStartup<Startup>()
        .Build();
    }
}
